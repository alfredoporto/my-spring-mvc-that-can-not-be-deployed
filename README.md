Topics presented in this small projects:
    Dependency injection
    Layered architecture
    JDBC connectivity (made simple)
    Spring framework
    Working with a database

Motivation: Reinforce concepts from my oriented-programming language  college course :)
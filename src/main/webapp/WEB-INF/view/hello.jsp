<%--
  Created by IntelliJ IDEA.
  User: alfredoportocarrero
  Date: 2019-09-13
  Time: 00:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %> <!--will disable EL for the whole JSP page.-->
<html>
<head>
    <title>Hello World Application</title>
</head>
<body>
    <h4>${message}</h4>
</body>
</html>

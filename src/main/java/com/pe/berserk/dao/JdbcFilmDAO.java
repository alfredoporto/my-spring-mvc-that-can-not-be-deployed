package com.pe.berserk.dao;

import com.pe.berserk.domain.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;


public class JdbcFilmDAO implements FilmDAO{

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void addFilm(Film film){
        jdbcTemplate.update("insert into films (code, title, did, date_prod, kind, len) values (?,?,?,?,?,?)",
                film.getCode(),
                film.getTitle(),
                film.getDid(),
                film.getDateProd(),
                film.getKind(),
                film.getLength());
    }
}

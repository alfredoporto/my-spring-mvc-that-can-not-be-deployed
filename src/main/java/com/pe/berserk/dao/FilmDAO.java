package com.pe.berserk.dao;

import com.pe.berserk.domain.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmDAO {
    void addFilm(Film film);
}

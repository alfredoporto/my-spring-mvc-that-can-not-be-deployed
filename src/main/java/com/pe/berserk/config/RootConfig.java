package com.pe.berserk.config;

import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.pe.berserk")
public class RootConfig {
}

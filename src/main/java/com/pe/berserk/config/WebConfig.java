package com.pe.berserk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


//@EnableWebMvc allows us to register a number of springMVC infrastructures beans that are going to support app concerns like requestMapping
@EnableWebMvc
@ComponentScan("com.pe.berserk")
public class WebConfig {

    @Bean //will register viewResolver as a bean within dispatcherServlet app context
    public InternalResourceViewResolver viewResolver(){
        //This object is used to access files within the web WEB-INF dir.
        //Content within WEB-INF can not be accessible directly.
        //The InternalResourceViewResolver can expose the content for our dispatcherServlet when it's rendering a view
        InternalResourceViewResolver viewResolverBean = new InternalResourceViewResolver();
        viewResolverBean.setPrefix("/WEB-INF/view/"); //that's the path before the Controller gives the view's identifier
        viewResolverBean.setSuffix(".jsp"); //that will be append to the path above
        return viewResolverBean;
    }

    @Bean
    public DriverManagerDataSource dataSource(){
        DriverManagerDataSource dataSourceBean = new DriverManagerDataSource();
        dataSourceBean.setDriverClassName("org.postgresql.Driver");
        dataSourceBean.setUrl("jdbc:postgresql://localhost:5432/jdbc");
        dataSourceBean.setUsername("alfredoportocarrero");
        dataSourceBean.setPassword("");
        return dataSourceBean;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(){
        JdbcTemplate jdbcTemplateBean = new JdbcTemplate();
        jdbcTemplateBean.setDataSource(dataSource());
        return jdbcTemplateBean;
    }
}

package com.pe.berserk.domain;

import org.postgresql.util.PGInterval;
import java.util.Date;

public class Film {
    private String code;
    private String title;
    private Integer did;
    private Date dateProd;
    private String kind;
    private PGInterval length;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public Date getDateProd() {
        return dateProd;
    }

    public void setDateProd(Date dateProd) {
        this.dateProd = dateProd;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public PGInterval getLength() {
        return length;
    }

    public void setLength(PGInterval length) {
        this.length = length;
    }
}

/* This a method on method to register the dispatcher, the other
is in DispatcherInitializer.java

package com.pe.berserk.initializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.*;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import com.pe.berserk.config.WebConfig;

//just to bootstrap the project
public class AppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //need to bootstrap my container, for that I will use an ApplicationContext
        //need to register the DispatcherServlet with the ServletContext

        //bootstrapping my container
        //this represents my container and allows to register beans and configurations with it
        //This a special type of Context that allows to use annotations on my classes to config the Container
        AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();

        //invoke register() method to register a config class
        dispatcherContext.register(WebConfig.class);

        //can be instantiated with its constructor and accepts an overloaded form that allows me to supply an appContext as an arg
        DispatcherServlet dispatcherServlet = new DispatcherServlet(dispatcherContext);

        //using Servlet 3.0 features we can now dynamically register the servlet
        ServletRegistration.Dynamic registration = servletContext.addServlet("dispatcher",dispatcherServlet);

        //supply the path that the servlet is mapped to, in this case we are mapping to "/" that will cause dispatcherServlet to handle
        //all requests that do not have an explicit mapping
        registration.addMapping("/");

        //it will cause that our dispatcherServlet to be the first servlet registered within this application
        registration.setLoadOnStartup(1);
    }
}

*/

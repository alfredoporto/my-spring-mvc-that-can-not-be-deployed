package com.pe.berserk.controller;

import com.pe.berserk.domain.Film;
import com.pe.berserk.service.InsertFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


//main responsible for routing the request to the corresponding controller
@Controller //@Controller will tell @ComponentScanner to register this class as a Controller bean
@RequestMapping("/main") //provides info to the dispatcherServlet that is used to map request to a particular handler method on a controller
public class MainController {

    @Autowired
    InsertFilmService insertFilmService;

    //in order  to have requests mapped to this handler method, we need to add the request mapping annotation
    //we are going to map it to "/", so any request that comes into the dispatcherServlet for /main / are going to cause this handler method
    //to be invoked


    @RequestMapping(value = "/insertFilm", method = RequestMethod.POST, params = {"type=code", "type=title", "type=did",
            "type=date_prod", "type=kind", "type=len"})
    public String insertFilm(@ModelAttribute Film film){
        this.insertFilmService.insertFilm(film);
        System.out.println("InsertFilmService worked");
        return "hello";

        /*
        model.addAttribute("message",this.insertFilmService.insertFilm());
        //return this.greetingService.insertFilm();

        return "hello"; //that's our view's name and that will be returned by the Controller to the dispatcherServlet that's going to consult
        // our internalResourceView... It's going to append hello after our prefix, and then it's going to add the suffix '.jsp'
        //and that's how are we going to identify the appropriate view.
        */
    }
}

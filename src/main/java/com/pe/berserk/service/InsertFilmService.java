package com.pe.berserk.service;

import com.pe.berserk.dao.FilmDAO;
import com.pe.berserk.dao.JdbcFilmDAO;
import com.pe.berserk.domain.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InsertFilmService {

    @Autowired
    FilmDAO dao;

    public void insertFilm(Film film){
        dao.addFilm(film);
    }
}

package com.pe.berserk;

import com.pe.berserk.config.RootConfig;
import com.pe.berserk.config.WebConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class DispatcherInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {RootConfig.class};
    }

    @Override
    //return an array of classes that are our configuration classes for the container.
    protected Class<?>[] getServletConfigClasses() {
        //view resolver is a web component, we have to register within my config as a bean
        return new Class[] {WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
